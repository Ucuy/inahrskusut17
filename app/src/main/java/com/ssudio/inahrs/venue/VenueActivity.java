package com.ssudio.inahrs.venue;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.ssudio.inahrs.R;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VenueActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BaseSliderView.OnSliderClickListener {

    private final String TAG = getClass().getSimpleName();
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.home_SliderLayout_promotion)
    SliderLayout mPromotionSlider;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.venue_TV_showDirection)
    void showDirection() {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=The+Westin+Jakarta,+Jakarta+Indonesia");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @OnClick(R.id.venue_TV_showLocation)
    void showLocation() {
        Uri gmmIntentUri = Uri.parse("geo:-6.2229211,106.8324422");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue);
        ButterKnife.bind(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        setupToolbar();
        setupHotelSlider();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void setupHotelSlider() {
        HashMap<String, String> imageUrls = new HashMap<>();
        imageUrls.put("Westin 1", "https://www.istania.net/blog/wp-content/uploads/2015/03/the-westin-jakarta-bar-interior-3d-picture.jpg");
        imageUrls.put("Westin 2", "http://www.starwoodhotels.com/pub/media/3868/wes3868re.178059_xx.jpg");
        imageUrls.put("Westin 3", "http://www.starwoodmediacentre.com/uploaded-images/chunks/cd00795f-a594-4261-9fde-c8ee41f1b77b-653x367.jpg");
        imageUrls.put("Westin 4", "http://www.starwoodhotels.com/pub/media/3868/wes3868lo.177020_xx.jpg");

        // Adding sliders to the container
        for (String name : imageUrls.keySet()) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(this);

            // initialize a SliderLayout
            defaultSliderView
                    .image(imageUrls.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            // add extra information
            defaultSliderView.bundle(new Bundle());
            defaultSliderView.getBundle().putString("extra", name);

            // add to slider
            mPromotionSlider.addSlider(defaultSliderView);
        }

        // Slider's container setting
        mPromotionSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mPromotionSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mPromotionSlider.setCustomAnimation(new DescriptionAnimation());
        mPromotionSlider.setDuration(5000);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: ");
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "onConnected: ");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d(TAG, "onConnected: " + mLastLocation.getLatitude());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}