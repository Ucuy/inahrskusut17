package com.ssudio.inahrs.aboutcongress;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.event.symposium.Symposium;
import com.ssudio.inahrs.event.symposium.SymposiumAdapter;
import com.ssudio.inahrs.event.workshop.Workshop;
import com.ssudio.inahrs.event.workshop.WorkshopAdapter;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.TinyDB;
import com.ssudio.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyCongressActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;

    @BindView(R.id.congress_RV_sympo)
    RecyclerView mSympoRV;
    @BindView(R.id.congress_RV_workshop)
    RecyclerView mWorkshopRV;

    List<Symposium> mSymposia = new ArrayList<>();
    List<Workshop> mWorkshops = new ArrayList<>();

    SymposiumAdapter mSymposiumAdapter;
    WorkshopAdapter mWorkshopAdapter;
    TinyDB mTinyDB;
    List<Integer> mBookmarkedSymposiumIds;
    List<Integer> mBookmarkedWorkshopIds;

    private String mPopupMessage;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_congress);
        ButterKnife.bind(this);


        Log.d("###", "onCreate: Eventchoiceactivty created");
        mPopupMessage = getIntent().getStringExtra("popup_message");

        if (mPopupMessage == null) {
            Log.d("###", "onCreate: There's no popup window.");
        } else {
            showPopup(mPopupMessage);
            Log.d("###", "onCreate: Show popup with message: " + mPopupMessage);
        }

        setupTinyDB();

        setupToolbar();
        setupRVs();

        fetchWorkshops();
        fetchSymposia();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedSymposiumIds = mTinyDB.getListInt(Config.TINYDB_SYMPO);
        mBookmarkedWorkshopIds = mTinyDB.getListInt(Config.TINYDB_WORKSHOP);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void setupRVs() {
        mSympoRV.setLayoutManager(new LinearLayoutManager(this));
        mSymposiumAdapter = new SymposiumAdapter(mSymposia);
        mSympoRV.setAdapter(mSymposiumAdapter);

        mWorkshopRV.setLayoutManager(new LinearLayoutManager(this));
        mWorkshopAdapter = new WorkshopAdapter(mWorkshops);
        mWorkshopRV.setAdapter(mWorkshopAdapter);
    }

    private void fetchSymposia() {
        String fetchURL = Config.SYMPOSIUM_GET_LIST;
        mSymposia.clear();
        JsonArrayRequest sympoListRequest = new JsonArrayRequest(
                fetchURL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);

                                if (mBookmarkedSymposiumIds.contains(object.getInt("id"))) {
                                    mSymposia.add(new Symposium(
                                            object.getInt("id"),
                                            object.getString("name"),
                                            Config.ENDPOINT + object.getString("image"),
                                            object.getString("description"),
                                            object.getString("date"),
                                            object.getString("start_time"),
                                            object.getString("end_time"),
                                            object.getString("venue")
                                    ));
                                }
                            }

                            mSymposiumAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Log.e("###", "onResponse: " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("###", "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(sympoListRequest);
    }

    private void fetchWorkshops() {
        mWorkshops.clear();
        JsonArrayRequest workshopListRequest = new JsonArrayRequest(
                Config.WORKSHOP_GET_LIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);

                                if (mBookmarkedWorkshopIds.contains(object.getInt("id"))) {
                                    mWorkshops.add(new Workshop(
                                            object.getInt("id"),
                                            object.getString("name"),
                                            Config.ENDPOINT + object.getString("image"),
                                            object.getString("description"),
                                            object.getString("date"),
                                            object.getString("start_time"),
                                            object.getString("end_time"),
                                            object.getString("venue")
                                    ));
                                }
                            }
                            mWorkshopAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Log.d("###", "onResponse: Error " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(workshopListRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showPopup(String mPopupMessage) {
        AlertDialog.Builder notificationPopupBuilder = new AlertDialog.Builder(this);
        notificationPopupBuilder.setMessage("One of your session will be held soon. Check your bookmarked sessions for further informations.");
        notificationPopupBuilder.setCancelable(true);

        notificationPopupBuilder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = notificationPopupBuilder.create();
        dialog.show();
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}
