package com.ssudio.inahrs.sponsor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SponsorListActivity extends AppCompatActivity {

    private static final String TAG = Config.TAG;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;

    @BindView(R.id.sponsorList_RV_goldSponsors)
    RecyclerView mGoldRV;
    @BindView(R.id.sponsorList_RV_silverSponsors)
    RecyclerView mSilverRV;

    @BindView(R.id.sponsorList_RV_bronzeSponsors)
    RecyclerView mBronzeRV;

    ProgressDialog mProgressDialog;

    private List<Sponsor> mBronzeSponsors = new ArrayList<>();
    private List<Sponsor> mSilverSponsors = new ArrayList<>();
    private List<Sponsor> mGoldSponsors = new ArrayList<>();
    private SponsorListItemAdapter mBronzeAdapter;
    private SponsorListItemAdapter mSilverAdapter;
    private SponsorListItemAdapter mGoldAdapter;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsor_list);
        ButterKnife.bind(this);

        setupProgressDialog();
        setupToolbar();
        populateSponsorListItem();
        setupRecyclerView();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
    }

    private void populateSponsorListItem() {
        Log.d("###", "populateSponsorListItem: ");
        mProgressDialog.show();
        mBronzeSponsors.clear();
        mSilverSponsors.clear();
        mGoldSponsors.clear();

        JsonArrayRequest sponsorListRequest = new JsonArrayRequest(
                Config.SPONSOR_GET_LIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {

                                Sponsor sponsor = new Sponsor(response.getJSONObject(i));

                                if (sponsor.getType().equals("bronze")) {
                                    mBronzeSponsors.add(sponsor);
                                } else if (sponsor.getType().equals("silver")) {
                                    mSilverSponsors.add(sponsor);
                                } else if (sponsor.getType().equals("gold")) {
                                    mGoldSponsors.add(sponsor);
                                }  else {
                                    // todo: does nothing for now
                                }
                            }

                            setupSponsors();
                            mProgressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(sponsorListRequest);
    }

    private void setupSponsors() {
        // For bronze sponsors, simply refresh the list
        mGoldAdapter.notifyItemRangeInserted(0, mGoldSponsors.size());

        // For silver sponsor, make it big, but make it clickable to go to its website
        mSilverAdapter.notifyItemRangeInserted(0, mSilverSponsors.size());

        // For gold sponsor, make it big and make it go into detail
        mBronzeAdapter.notifyItemRangeInserted(0, mBronzeSponsors.size());
    }

    private void setupRecyclerView() {
        mBronzeRV.setHasFixedSize(true);
        mGoldAdapter = new SponsorListItemAdapter(mGoldSponsors);
        mGoldRV.setAdapter(mGoldAdapter);
        mGoldRV.setLayoutManager(new LinearLayoutManager(this));

        mBronzeRV.setHasFixedSize(true);
        mSilverAdapter = new SponsorListItemAdapter(mSilverSponsors);
        mSilverRV.setAdapter(mSilverAdapter);
        mSilverRV.setLayoutManager(new LinearLayoutManager(this));

        mBronzeRV.setHasFixedSize(true);
        mBronzeAdapter = new SponsorListItemAdapter(mBronzeSponsors);
        mBronzeRV.setLayoutManager(new GridLayoutManager(this, 2));
        mBronzeRV.setAdapter(mBronzeAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}
