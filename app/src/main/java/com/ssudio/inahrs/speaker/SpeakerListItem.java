package com.ssudio.inahrs.speaker;

public class SpeakerListItem {

    private int mId;
    private String mImageURL;
    private String mName;
    private String mNationality;
    private String mTitle;

    public SpeakerListItem(int id, String imageURL, String name, String nationality, String title) {
        mId = id;
        mImageURL = imageURL;
        mName = name;
        mNationality = nationality;
        mTitle = title;
    }

    public int getId() {
        return mId;
    }

    public String getImageURL() {
        return mImageURL;
    }

    public String getName() {
        return mName;
    }

    public String getNationality() {
        return mNationality;
    }

    public String getTitle() {
        return mTitle;
    }
}
