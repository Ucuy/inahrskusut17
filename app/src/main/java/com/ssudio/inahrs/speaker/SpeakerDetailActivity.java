package com.ssudio.inahrs.speaker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SpeakerDetailActivity extends AppCompatActivity {

    public static final String SPEAKER_DETAIL_ID_INTENT = "speaker_id";
    private static final String TAG = Config.TAG;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.speakerDetail_IV_image)
    ImageView mSpeakerImageView;
    @BindView(R.id.speakerDetail_TV_name)
    TextView mNameTextView;
    @BindView(R.id.speakerDetail_TV_title)
    TextView mTitleTextView;
    @BindView(R.id.speakerDetail_TV_nationality)
    TextView mNationalityTextView;
    @BindView(R.id.speakerDetail_TV_description)
    TextView mDescriptionTextView;
    ProgressDialog mProgressDialog;
    private int mSpeakerId;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_detail);
        ButterKnife.bind(this);
        mSpeakerId = getIntent().getIntExtra(SPEAKER_DETAIL_ID_INTENT, 0);

        setupProgressDialog();
        setupToolbar();
        fetchSpeakerDetail();
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void fetchSpeakerDetail() {
        mProgressDialog.show();
        JsonArrayRequest speakerDetailRequest = new JsonArrayRequest(
                Config.SPEAKER_ENDPOINT + String.valueOf(mSpeakerId),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            updateDetailView(response);
                        } catch (JSONException e) {
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }
                        mProgressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SpeakerDetailActivity.this, "Something went wrong. Check your network connection.", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(speakerDetailRequest);
    }

    private void updateDetailView(JSONArray response) throws JSONException {
        JSONObject object = response.getJSONObject(0);

        // use polygon
        Glide.with(this).load(Config.ENDPOINT + object.getString("speaker_image_polygon"))
                .into(mSpeakerImageView);
        mNameTextView.setText(object.getString("speaker_name"));
        mTitleTextView.setText(object.getString("speaker_title"));
        mNationalityTextView.setText(object.getString("speaker_nationality"));
        mDescriptionTextView.setText(object.getString("speaker_description"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}