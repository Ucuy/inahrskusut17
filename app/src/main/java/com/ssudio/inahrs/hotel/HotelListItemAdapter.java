package com.ssudio.inahrs.hotel;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HotelListItemAdapter extends RecyclerView.Adapter<HotelListItemAdapter.ViewHolder> {

    List<HotelListItem> mHotelListItems;

    public HotelListItemAdapter(List<HotelListItem> hotelListItems) {
        mHotelListItems = hotelListItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hotel_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HotelListItem hotelListItem = mHotelListItems.get(position);

        holder.mNameTextView.setText(hotelListItem.getName());
        holder.mDistanceTextView.setText(hotelListItem.getDistance() + " km");
        Glide.with(holder.mHotelImageView.getContext())
                .load(hotelListItem.getImageUrl())
                .into(holder.mHotelImageView);
    }

    @Override
    public int getItemCount() {
        return mHotelListItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.hotelList_TV_name)
        TextView mNameTextView;
        @BindView(R.id.hotelList_TV_distance)
        TextView mDistanceTextView;
        @BindView(R.id.hotelList_IV_image)
        ImageView mHotelImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    HotelListItem hotelListItem = mHotelListItems.get(getLayoutPosition());

                    Context c = view.getContext();
                    Intent intent = new Intent(c, HotelDetailActivity.class);
                    intent.putExtra(HotelDetailActivity.HOTEL_INDEX, hotelListItem.getId());
                    c.startActivity(intent);
                }
            });
        }
    }
}
