package com.ssudio.inahrs.hotel;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HotelDetailActivity extends AppCompatActivity {

    public static final String HOTEL_INDEX = "com.com.ssudio.inahrs.hotel_index";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.hotelDetail_IV_image)
    ImageView mHotelImageView;
    @BindView(R.id.hotelDetail_TV_name)
    TextView mNameTextView;
    @BindView(R.id.hotelDetail_TV_distance)
    TextView mDistanceTextView;
    @BindView(R.id.hotelDetail_TV_price)
    TextView mPriceTextView;
    @BindView(R.id.hotelDetail_TV_detail)
    TextView mDetailTextView;
    @BindView(R.id.hotelDetail_TV_address)
    TextView mAddressTextView;
    @BindView(R.id.hotelDetail_TV_phone)
    TextView mPhoneTextView;
    @BindView(R.id.hotelDetail_TV_website)
    TextView mWebsiteTextView;
    String mPhoneNumber = null;
    String mWebsiteAddress = null;
    private int mHotelId;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);
        ButterKnife.bind(this);
        mHotelId = getIntent().getIntExtra(HOTEL_INDEX, 0);

        setupToolbar();
        loadBannerImage();
        fetchHotelDetail();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder).
                into(mBannerImageView);
    }

    private void fetchHotelDetail() {
        JsonArrayRequest hotelDetailRequest = new JsonArrayRequest(
                Config.HOTEL_ENDPOINT + String.valueOf(mHotelId),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            updateDetailView(response);
                        } catch (JSONException e) {
                            Log.e("###", "onResponse: " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("###", "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(hotelDetailRequest);
    }

    private void updateDetailView(JSONArray response) throws JSONException {
        JSONObject object = response.getJSONObject(0);

        Glide.with(this).load(Config.ENDPOINT + object.getString("hotel_image"))
                .into(mHotelImageView);
        mNameTextView.setText(object.getString("hotel_name"));
        mDistanceTextView.setText(object.getString("hotel_distance") + "km from the venue");

        mPriceTextView.setText(object.getString("hotel_price") + "/night");
        mAddressTextView.setText(object.getString("hotel_alamat"));
        mPhoneTextView.setText(object.getString("hotel_telepon"));
        mPhoneNumber = object.getString("hotel_telepon");
        mWebsiteTextView.setText(object.getString("hotel_website"));
        mWebsiteAddress = object.getString("hotel_website");
        mDetailTextView.setText(object.getString("hotel_detail"));
    }

    @OnClick(R.id.hotelDetail_TV_phone)
    void callNumber() {
        String uri = "tel:" + mPhoneNumber.trim();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    @OnClick(R.id.hotelDetail_TV_website)
    void gotoWebsite() {
        Uri uriUrl = Uri.parse(mWebsiteAddress);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

}
