package com.ssudio.inahrs.hotel;

public class HotelListItem {

    private int mId;
    private String mName;
    private String mImageUrl;
    private String mDistance;

    public HotelListItem(int id, String name, String imageUrl, String distance) {
        mId = id;
        mName = name;
        mImageUrl = imageUrl;
        mDistance = distance;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getDistance() {
        return mDistance;
    }
}
