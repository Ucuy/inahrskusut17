package com.ssudio.inahrs.event;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.ssudio.inahrs.R;
import com.ssudio.inahrs.aboutcongress.MyCongressActivity;
import com.ssudio.inahrs.util.Config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EventAlarmService extends IntentService {

    private static final String TAG = Config.TAG;

    public EventAlarmService() {
        super(Config.TAG);
    }

    /**
     * This sets the alarm and
     * <p>
     * DATE format should be "YYYY-MM-DD"
     * TIME format should be "hh:mm"
     */

    public static void setAlarm(Context context, int notifId, String name, String date, String time, String venue) {

        // The intent to start an alarm
        Intent alarmIntent = new Intent(context, EventAlarmService.class);
        alarmIntent.putExtra("id", notifId);
        alarmIntent.putExtra("title", name);
        alarmIntent.putExtra("body", date + ", " + time + ", " + venue);
        alarmIntent.putExtra("message", venue);

        // PendingIntent is the wrapper intent
        PendingIntent notifPendingIntent = PendingIntent.getService(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        String datetime = date + " " + time;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

        Calendar alarmCalendar = Calendar.getInstance();
        try {

            // di sini bisa ngeset kapan notifnya mau ditampilin
            alarmCalendar.setTime(dateFormat.parse(datetime));

            int millis = 1000;
            int seconds = 60;
            int minutes = 60;

            alarmManager.set(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis() - (millis * seconds * 120), notifPendingIntent); // 2 hours before
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis() - (millis * seconds * 60), notifPendingIntent); // an hour before
            alarmManager.set(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis() - (millis * seconds * 15), notifPendingIntent); // 15 minutes before
        } catch (ParseException e) {
            Log.e(TAG, "setAlarm: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String title = intent.getStringExtra("title");
        String body = intent.getStringExtra("body");
        String message = intent.getStringExtra("message");

        int random = 10000 + (int) (Math.random() * ((99999 - 10000) + 1));
        notifyEventAboutToStart(title, body, message, random);
    }

    private void notifyEventAboutToStart(String title, String body, String popupMessage, int notifId) {
        // The intent to change to see Event Choices
        Intent onNotifClickedIntent = new Intent(getApplicationContext(), MyCongressActivity.class);
        onNotifClickedIntent.putExtra("popup_message", popupMessage);

        // To make sure the intent lasts a lifetime of the app
        PendingIntent onNotifClickedPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, onNotifClickedIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder)
                new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setContentIntent(onNotifClickedPendingIntent)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(notifId, mBuilder.build());
    }
}
