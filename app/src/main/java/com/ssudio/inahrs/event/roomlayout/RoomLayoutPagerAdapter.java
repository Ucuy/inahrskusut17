package com.ssudio.inahrs.event.roomlayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class RoomLayoutPagerAdapter extends FragmentPagerAdapter {

    final private int PAGE_COUNT = 2;

    private String[] pageTitles = {"Ground Floor", "First Floor"};

    public RoomLayoutPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return GroundFloorFragment.newInstance();
            case 1:
                return FirstFloorFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitles[position];
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
