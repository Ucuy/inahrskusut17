package com.ssudio.inahrs.event.workshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorkshopDetailHackActivity extends AppCompatActivity {

    public static final String EVENT_DETAIL_INDEX = "com.example.com.ssudio.inahrs.event_detail";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;

    @BindView(R.id.titleImage)
    ImageView mTitleImage;
    @BindView(R.id.workshopBookmark)
    ImageView mWorkshopBookmark;
    @BindView(R.id.workshopContent)
    ImageView mWorkshopContent;

    TinyDB mTinyDB;
    ArrayList<Integer> mBookmarkedWorkshopIds = new ArrayList<>();
    private int mWorkshopId;
    private boolean isBookmarked = false;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.askQuestionButton)
    void gotoAskQuestion() {
        Intent intent = new Intent(this, AskWorkshopQuestionActivity.class);
        intent.putExtra(AskWorkshopQuestionActivity.WORKSHOP_ID, mWorkshopId);
        startActivity(intent);
    }

    @OnClick(R.id.workshopBookmark)
    void onBookmarkClicked() {
        if (!isBookmarked) addBookmark();
        else removeBookmark();
    }

    private void addBookmark() {
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
        mBookmarkedWorkshopIds.add(mWorkshopId);
        mTinyDB.putListInt(Config.TINYDB_WORKSHOP, mBookmarkedWorkshopIds);
        isBookmarked = true;
    }

    private void removeBookmark() {
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_inactive));
        for (int i = 0; i < mBookmarkedWorkshopIds.size(); i++)
            if (mBookmarkedWorkshopIds.get(i) == mWorkshopId)
                mBookmarkedWorkshopIds.remove(i);
        mTinyDB.putListInt(Config.TINYDB_WORKSHOP, mBookmarkedWorkshopIds);
        isBookmarked = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symposium_hack_detail);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();
        mWorkshopId = getIntent().getIntExtra(EVENT_DETAIL_INDEX, 0);
        updateView();

        setupTinyDB();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedWorkshopIds = mTinyDB.getListInt(Config.TINYDB_WORKSHOP);

        isBookmarked = mBookmarkedWorkshopIds.contains(mWorkshopId);
        if (isBookmarked)
            mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private void updateView() {

        int contentResource = 0;
        int titleResource = 0;

        int[] contentDrawables = {
                R.drawable.content1,
                R.drawable.content2,
                R.drawable.content3,
                R.drawable.content4,
                R.drawable.content5,
                R.drawable.content6,
                R.drawable.content7,
                R.drawable.content8,
        };

        int[] titleDrawables = {
                R.drawable.header1,
                R.drawable.header2,
                R.drawable.header3,
                R.drawable.header4,
                R.drawable.header5,
                R.drawable.header6,
                R.drawable.header7,
                R.drawable.header8,
        };

        switch (mWorkshopId) {
            case 1:
                contentResource = contentDrawables[0];
                titleResource = titleDrawables[0];
                break;
            case 2:
                contentResource = contentDrawables[1];
                titleResource = titleDrawables[1];
                break;
            case 3:
                contentResource = contentDrawables[2];
                titleResource = titleDrawables[2];
                break;
            case 4:
                contentResource = contentDrawables[3];
                titleResource = titleDrawables[3];
                break;
            case 5:
                contentResource = contentDrawables[4];
                titleResource = titleDrawables[4];
                break;
            case 6:
                contentResource = contentDrawables[5];
                titleResource = titleDrawables[5];
                break;
            case 7:
                contentResource = contentDrawables[6];
                titleResource = titleDrawables[6];
                break;
            case 9:
                contentResource = contentDrawables[7];
                titleResource = titleDrawables[7];
                break;
        }

        Glide.with(this).load(contentResource).into(mWorkshopContent);
        Glide.with(this).load(titleResource).into(mTitleImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}