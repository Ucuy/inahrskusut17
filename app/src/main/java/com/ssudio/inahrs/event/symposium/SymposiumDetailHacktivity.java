package com.ssudio.inahrs.event.symposium;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.TinyDB;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymposiumDetailHacktivity extends AppCompatActivity {

    public static final String EVENT_DETAIL_INDEX = "com.example.com.ssudio.inahrs.event_detail";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;

    @BindView(R.id.titleImage)
    ImageView mTitleImage;
    @BindView(R.id.workshopBookmark)
    ImageView mWorkshopBookmark;
    @BindView(R.id.workshopContent)
    ImageView mWorkshopContent;

    TinyDB mTinyDB;
    ArrayList<Integer> mBookmarkedSymposiumIds = new ArrayList<>();
    private int mSymposiumId;
    private boolean isBookmarked = false;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.askQuestionButton)
    void gotoAskQuestion() {
        Intent intent = new Intent(this, AskSympoQuestionActivity.class);
        intent.putExtra(AskSympoQuestionActivity.SYMPOSIUM_ID, mSymposiumId);
        startActivity(intent);
    }

    @OnClick(R.id.workshopBookmark)
    void onBookmarkClicked() {
        Log.d("###", "onBookmarkClicked: Clicked!");
        if (!isBookmarked) addBookmark();
        else removeBookmark();
    }

    private void addBookmark() {
        Log.d("###", "addBookmark: Adding bookmark.");
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
        mBookmarkedSymposiumIds.add(mSymposiumId);
        Log.d("###", "addBookmark: adding " + mSymposiumId + " into list.");
        mTinyDB.putListInt(Config.TINYDB_SYMPO, mBookmarkedSymposiumIds);
        isBookmarked = true;
    }

    private void removeBookmark() {
        Log.d("###", "removeBookmark: Removing bookmark.");
        mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_inactive));
        Log.d("###", "addBookmark: removing" + mSymposiumId + " from list.");
        for (int i = 0; i < mBookmarkedSymposiumIds.size(); i++)
            if (mBookmarkedSymposiumIds.get(i) == mSymposiumId)
                mBookmarkedSymposiumIds.remove(i);
        mTinyDB.putListInt(Config.TINYDB_SYMPO, mBookmarkedSymposiumIds);
        isBookmarked = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symposium_hack_detail);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();
        mSymposiumId = getIntent().getIntExtra(EVENT_DETAIL_INDEX, 0);
        setupTinyDB();

        updateView();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedSymposiumIds = mTinyDB.getListInt(Config.TINYDB_SYMPO);
        isBookmarked = mBookmarkedSymposiumIds.contains(mSymposiumId);
        if (isBookmarked)
            mWorkshopBookmark.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_big_active));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private void updateView() {

        int contentResource = 0;
        int titleResource = 0;

        int[] fridayTitle = {
                R.drawable.sympo_friday_header1,
                R.drawable.sympo_friday_header2,
                R.drawable.sympo_friday_header3,
                R.drawable.sympo_friday_header4,
                R.drawable.sympo_friday_header5,
                R.drawable.sympo_friday_header6,
        };

        int[] fridayContent = {
                R.drawable.sympo_friday_content1,
                R.drawable.sympo_friday_content2,
                R.drawable.sympo_friday_content3,
                R.drawable.sympo_friday_content4,
                R.drawable.sympo_friday_content5,
                R.drawable.sympo_friday_content6,
        };

        int[] saturdayTitle = {
                R.drawable.sympo_saturday_header1,
                R.drawable.sympo_saturday_header2,
                R.drawable.sympo_saturday_header3,
                R.drawable.sympo_saturday_header4,
                R.drawable.sympo_saturday_header5,
                R.drawable.sympo_saturday_header6,
                R.drawable.sympo_saturday_header7,
                R.drawable.sympo_saturday_header8,
                R.drawable.sympo_saturday_header9,
                R.drawable.sympo_saturday_header10,
                R.drawable.sympo_saturday_header11,
                R.drawable.sympo_saturday_header12,
                R.drawable.sympo_saturday_header13,
                R.drawable.sympo_saturday_header14,
                R.drawable.sympo_saturday_header15,
                R.drawable.sympo_saturday_header16,
        };

        int[] saturdayContent = {
                R.drawable.sympo_saturday_content1,
                R.drawable.sympo_saturday_content2,
                R.drawable.sympo_saturday_content3,
                R.drawable.sympo_saturday_content4,
                R.drawable.sympo_saturday_content5,
                R.drawable.sympo_saturday_content6,
                R.drawable.sympo_saturday_content7,
                R.drawable.sympo_saturday_content8,
                R.drawable.sympo_saturday_content9,
                R.drawable.sympo_saturday_content10,
                R.drawable.sympo_saturday_content11,
                R.drawable.sympo_saturday_content12,
                R.drawable.sympo_saturday_content12,
                R.drawable.sympo_saturday_content14,
                R.drawable.sympo_saturday_content15,
                R.drawable.sympo_saturday_content16,
        };

        // todo: remove this
        int[] titleDrawables = {
                R.drawable.plenarylectures_title,
                R.drawable.arryht_upd_title,
                R.drawable.specialpopulation_title,
                R.drawable.suddendeath_title,
                R.drawable.breakfast1_title,
                R.drawable.breakfast2_title,
                R.drawable.plenarylectures_title,
                R.drawable.afib_tittle,
                R.drawable.supraventicular_title,
                R.drawable.lunchanticoagulant_title,
                R.drawable.lunchpacemaker_title,
                R.drawable.syncope_title,
                R.drawable.tachy_title,
                R.drawable.deivcetherapy_title,
                R.drawable.heartfailure_title,
                R.drawable.medicalaf_title,
                R.drawable.unknown_tracing_ecg_title
        };

        int[] contentDrawables = {
                R.drawable.plenarylectures_content,
                R.drawable.arryht_upd_content,
                R.drawable.specialpopulation_content,
                R.drawable.suddendeath_content,
                R.drawable.breakfast1_content,
                R.drawable.breakfast2_content,
                R.drawable.plenarylectures_content,
                R.drawable.afib_content,
                R.drawable.supraventicular_content,
                R.drawable.lunchanticoagulant_content,
                R.drawable.lunchpacemaker_content,
                R.drawable.syncope_content,
                R.drawable.tachy_content,
                R.drawable.devicetherapy_content,
                R.drawable.heartfailure_content,
                0,
                0
        };

        // todo: end remove this

        switch (mSymposiumId) {
//            case 1:
//                contentResource = contentDrawables[0];
//                titleResource = titleDrawables[0];
//                break;

            // friday
            case 9:
                contentResource = fridayContent[0];
                titleResource = fridayTitle[0];
                break;
            case 10:
                contentResource = fridayContent[1];
                titleResource = fridayTitle[1];
                break;
            case 11:
                contentResource = fridayContent[2];
                titleResource = fridayTitle[2];
                break;
            case 12:
                contentResource = fridayContent[3];
                titleResource = fridayTitle[3];
                break;
            case 13:
                contentResource = fridayContent[4];
                titleResource = fridayTitle[4];
                break;
            case 16:
                contentResource = fridayContent[5];
                titleResource = fridayTitle[5];
                break;

            // saturday
            case 15:
                contentResource = saturdayContent[0];
                titleResource = saturdayTitle[0];
                break;
            case 17:
                contentResource = saturdayContent[1];
                titleResource = saturdayTitle[1];
                break;
            case 18:
                contentResource = saturdayContent[2];
                titleResource = saturdayTitle[2];
                break;
            case 19:
                contentResource = saturdayContent[3];
                titleResource = saturdayTitle[3];
                break;
            case 20:
                contentResource = saturdayContent[4];
                titleResource = saturdayTitle[4];
                break;
            case 21:
                contentResource = saturdayContent[5];
                titleResource = saturdayTitle[5];
                break;
            case 22:
                contentResource = saturdayContent[6];
                titleResource = saturdayTitle[6];
                break;
            case 23:
                contentResource = saturdayContent[7];
                titleResource = saturdayTitle[7];
                break;
            case 24:
                contentResource = saturdayContent[8];
                titleResource = saturdayTitle[8];
                break;
            case 25:
                contentResource = saturdayContent[9];
                titleResource = saturdayTitle[9];
                break;
            case 26:
                contentResource = saturdayContent[10];
                titleResource = saturdayTitle[10];
                break;
            case 28:
                contentResource = saturdayContent[11];
                titleResource = saturdayTitle[11];
                break;
            case 29:
                contentResource = saturdayContent[12];
                titleResource = saturdayTitle[12];
                break;
            case 30:
                contentResource = saturdayContent[13];
                titleResource = saturdayTitle[13];
                break;
            case 31:
                contentResource = saturdayContent[14];
                titleResource = saturdayTitle[14];
                break;
            case 32:
                contentResource = saturdayContent[15];
                titleResource = saturdayTitle[15];
                break;
        }

        Glide.with(this).load(contentResource).into(mWorkshopContent);
        Glide.with(this).load(titleResource).into(mTitleImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
