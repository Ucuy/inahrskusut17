package com.ssudio.inahrs.event.symposium;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.TinyDB;
import com.ssudio.inahrs.util.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymposiumDetailActivity extends AppCompatActivity {

    public static final String EVENT_DETAIL_INDEX = "com.example.com.ssudio.inahrs.event_detail";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;
    @BindView(R.id.symposiumDetail_TV_name)
    TextView mName;
    @BindView(R.id.symposiumDetail_IV_picture)
    ImageView mPicture;
    @BindView(R.id.symposiumDetail_TV_description)
    TextView mDescription;
    @BindView(R.id.symposiumDetail_TV_date)
    TextView mDate;
    @BindView(R.id.symposiumDetail_TV_startEndTime)
    TextView mStartEndTime;
    @BindView(R.id.symposiumDetail_TV_venue)
    TextView mVenue;

    @BindView(R.id.eventDetail_LL_bookmark)
    LinearLayout mBookmarkLL;
    @BindView(R.id.eventDetail_IV_bookmarkIcon)
    ImageView mBookmarkIcon;
    TinyDB mTinyDB;
    ArrayList<Integer> mBookmarkedSymposiumIds = new ArrayList<>();
    private int mSymposiumId;
    private boolean isBookmarked = false;

    @OnClick(R.id.toolbar_back)
    void finishing() {
        finish();
    }

    @OnClick(R.id.symposiumDetail_FAB_question)
    void gotoAskQuestion() {
        Intent intent = new Intent(this, AskSympoQuestionActivity.class);
        intent.putExtra(AskSympoQuestionActivity.SYMPOSIUM_ID, mSymposiumId);
        startActivity(intent);
    }

    @OnClick(R.id.eventDetail_LL_bookmark)
    void onBookmarkClicked() {
        if (!isBookmarked) addBookmark();
        else removeBookmark();
    }

    private void addBookmark() {
        mBookmarkIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_active));
        mBookmarkedSymposiumIds.add(mSymposiumId);
        mTinyDB.putListInt(Config.TINYDB_SYMPO, mBookmarkedSymposiumIds);
        isBookmarked = true;
    }

    private void removeBookmark() {
        mBookmarkIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_inactive));
        for (int i = 0; i < mBookmarkedSymposiumIds.size(); i++)
            if (mBookmarkedSymposiumIds.get(i) == mSymposiumId)
                mBookmarkedSymposiumIds.remove(i);
        mTinyDB.putListInt(Config.TINYDB_SYMPO, mBookmarkedSymposiumIds);
        isBookmarked = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symposium_detail);
        ButterKnife.bind(this);

        setupToolbar();
        loadBannerImage();
        mSymposiumId = getIntent().getIntExtra(EVENT_DETAIL_INDEX, 0);
        fetchWorkshopDetail();

        setupTinyDB();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedSymposiumIds = mTinyDB.getListInt(Config.TINYDB_SYMPO);

        isBookmarked = mBookmarkedSymposiumIds.contains(mSymposiumId);
        if (isBookmarked)
            mBookmarkIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmark_active));
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
    }

    private void loadBannerImage() {
        Glide.with(this).
                load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private void fetchWorkshopDetail() {
        JsonArrayRequest eventDetailRequest = new JsonArrayRequest(
                Config.SYMPOSIUM_ENDPOINT + String.valueOf(mSymposiumId),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        JSONObject object;
                        try {
                            object = response.getJSONObject(0);
                            updateView(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("###", "onErrorResponse: " + error.getMessage());
                    }
                }
        );

        VolleySingleton.getInstance(this).addToRequestQueue(eventDetailRequest);
    }

    private void updateView(JSONObject object) throws JSONException {
        mName.setText(object.getString("symposium_name"));
        mDescription.setText(Html.fromHtml(object.getString("symposium_description")));
        mDate.setText(object.getString("symposium_date"));
        Glide.with(this).load("http://www.puriclassicfurniture.com/wp-content/uploads/2013/07/Solo-paragon-meeting-room1.jpg").into(mPicture);
        mStartEndTime.setText(object.getString("symposium_start_time") + "-" + object.getString("symposium_end_time"));
        mVenue.setText(object.getString("symposium_venue"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
