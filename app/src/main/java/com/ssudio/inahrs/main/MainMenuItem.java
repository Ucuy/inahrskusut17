package com.ssudio.inahrs.main;

public class MainMenuItem {

    private int mId;
    private String mMenuTitle;
    private int mImageResourceId;

    public MainMenuItem(int id, String menuTitle, int imageResourceId) {
        mId = id;
        mMenuTitle = menuTitle;
        mImageResourceId = imageResourceId;
    }

    public int getId() {
        return mId;
    }

    public String getMenuTitle() {
        return mMenuTitle;
    }

    public int getImageResourceId() {
        return mImageResourceId;
    }
}
