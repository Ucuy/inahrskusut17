package com.ssudio.inahrs.main;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.aboutcongress.AskusActivity;
import com.ssudio.inahrs.aboutcongress.IntroductionActivity;
import com.ssudio.inahrs.aboutcongress.MyCongressActivity;
import com.ssudio.inahrs.event.EventChoiceActivity;
import com.ssudio.inahrs.hotel.HotelListActivity;
import com.ssudio.inahrs.program.ProgramListActivity;
import com.ssudio.inahrs.speaker.SpeakerListActivity;
import com.ssudio.inahrs.sponsor.SponsorListActivity;
import com.ssudio.inahrs.venue.VenueActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuItemAdapter extends RecyclerView.Adapter<MenuItemAdapter.ViewHolder> {

    public static final int MENU_ITEM_INDEX_INTRODUCTION = 1;
    public static final int MENU_ITEM_INDEX_MY_CONGRESS = 2;
    public static final int MENU_ITEM_INDEX_PROGRAMS = 3;
    public static final int MENU_ITEM_INDEX_EVENTS = 4;
    public static final int MENU_ITEM_INDEX_SPEAKERS = 5;

    public static final int MENU_ITEM_INDEX_SPONSORS = 6;
    public static final int MENU_ITEM_INDEX_ASK_US = 7;
    public static final int MENU_ITEM_INDEX_VENUE = 8;
    public static final int MENU_ITEM_INDEX_HOTELS = 9;

    List<MainMenuItem> mMainMenuItems;

    public MenuItemAdapter(List<MainMenuItem> mainMenuItems) {
        mMainMenuItems = mainMenuItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MainMenuItem mainMenuItem = mMainMenuItems.get(position);

        Glide.with(holder.iconImageView.getContext())
                .load(mainMenuItem.getImageResourceId())
                .into(holder.iconImageView);
        holder.titleTV.setText(mainMenuItem.getMenuTitle());
    }

    @Override
    public int getItemCount() {
        return mMainMenuItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.menuItem_IV_icon)
        ImageView iconImageView;
        @BindView(R.id.menuItem_TV_title)
        TextView titleTV;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Context c = v.getContext();
                    MainMenuItem item = mMainMenuItems.get(getLayoutPosition());

                    switch (item.getId()) {
                        case MENU_ITEM_INDEX_INTRODUCTION:
                            c.startActivity(new Intent(c, IntroductionActivity.class));
                            break;
                        case MENU_ITEM_INDEX_MY_CONGRESS:
                            c.startActivity(new Intent(c, MyCongressActivity.class));
                            break;
                        case MENU_ITEM_INDEX_PROGRAMS:
                            c.startActivity(new Intent(c, ProgramListActivity.class));
                            break;
                        case MENU_ITEM_INDEX_EVENTS:
                            c.startActivity(new Intent(c, EventChoiceActivity.class));
                            break;
                        case MENU_ITEM_INDEX_SPEAKERS:
                            c.startActivity(new Intent(c, SpeakerListActivity.class));
                            break;
                        case MENU_ITEM_INDEX_SPONSORS:
                            c.startActivity(new Intent(c, SponsorListActivity.class));
                            break;
                        case MENU_ITEM_INDEX_ASK_US:
                            c.startActivity(new Intent(c, AskusActivity.class));
                            break;
                        case MENU_ITEM_INDEX_VENUE:
                            c.startActivity(new Intent(c, VenueActivity.class));
                            break;
                        case MENU_ITEM_INDEX_HOTELS:
                            c.startActivity(new Intent(c, HotelListActivity.class));
                    }
                }
            });
        }
    }
}
