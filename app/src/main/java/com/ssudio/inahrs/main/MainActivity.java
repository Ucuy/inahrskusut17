package com.ssudio.inahrs.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.ssudio.inahrs.R;
import com.ssudio.inahrs.aboutcongress.AskusActivity;
import com.ssudio.inahrs.aboutcongress.IntroductionActivity;
import com.ssudio.inahrs.aboutcongress.MyCongressActivity;
import com.ssudio.inahrs.event.EventAlarmService;
import com.ssudio.inahrs.event.EventChoiceActivity;
import com.ssudio.inahrs.hotel.HotelListActivity;
import com.ssudio.inahrs.profile.ProfileActivity;
import com.ssudio.inahrs.program.ProgramListActivity;
import com.ssudio.inahrs.speaker.SpeakerListActivity;
import com.ssudio.inahrs.sponsor.SponsorListActivity;
import com.ssudio.inahrs.util.Config;
import com.ssudio.inahrs.util.TinyDB;
import com.ssudio.inahrs.util.VolleySingleton;
import com.ssudio.inahrs.venue.VenueActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.banner)
    ImageView mBannerImageView;

    @BindView(R.id.main_DrawerLayout)
    DrawerLayout mDrawer;
    @BindView(R.id.main_NavigationView)
    NavigationView mNavigationView;
    ActionBarDrawerToggle mDrawerToggle;

    @BindView(R.id.mainActivity_RV_menu)
    RecyclerView mMenuItemRV;
    List<MainMenuItem> mMainMenuItems;

    TinyDB mTinyDB;
    List<Integer> mBookmarkedSymposiumIds;
    List<Integer> mBookmarkedWorkshopIds;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupToolbar();
        setupNavDrawer();
        loadBannerImage();
        populateMenuItems();
        setupRecyclerView();

        setupTinyDB();
        fetchAlarms();
    }

    private void setupTinyDB() {
        mTinyDB = new TinyDB(this.getApplicationContext());
        mBookmarkedSymposiumIds = mTinyDB.getListInt(Config.TINYDB_SYMPO);
        mBookmarkedWorkshopIds = mTinyDB.getListInt(Config.TINYDB_WORKSHOP);
    }


    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setupNavDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
    }

    private void selectDrawerItem(MenuItem menuItem) {
        switch (menuItem.getItemId()) {

            case R.id.navView_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case R.id.navView_intro:
                startActivity(new Intent(this, IntroductionActivity.class));
                break;
            case R.id.navView_about:
                startActivity(new Intent(this, MyCongressActivity.class));
                break;
            case R.id.navView_program:
                startActivity(new Intent(this, EventChoiceActivity.class));
                break;
            case R.id.navView_events:
                startActivity(new Intent(this, ProgramListActivity.class));
                break;
            case R.id.navView_speakers:
                startActivity(new Intent(this, SpeakerListActivity.class));
                break;
            case R.id.navView_sponsors:
                startActivity(new Intent(this, SponsorListActivity.class));
                break;
            case R.id.navView_askus:
                startActivity(new Intent(this, AskusActivity.class));
                break;
            case R.id.navView_venue:
                startActivity(new Intent(this, VenueActivity.class));
                break;
            case R.id.navView_hotels:
                startActivity(new Intent(this, HotelListActivity.class));
                break;
        }

        mDrawer.closeDrawers();
    }

    private void loadBannerImage() {
        Glide.with(this)
                .load(R.drawable.header_banner_without_pic_placeholder)
                .into(mBannerImageView);
    }

    private void populateMenuItems() {
        mMainMenuItems = new ArrayList<>();
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_INTRODUCTION, "Introduction", R.drawable.icon_intro));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_MY_CONGRESS, "My Congress", R.drawable.icon_about));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_EVENTS, "Program", R.drawable.icon_events));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_SPEAKERS, "Speakers", R.drawable.icon_speakers));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_SPONSORS, "Sponsors", R.drawable.icon_handshake));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_VENUE, "Venue", R.drawable.icon_direction));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_HOTELS, "Hotels", R.drawable.icon_hotels));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_ASK_US, "Ask Us", R.drawable.icon_ask_us));
        mMainMenuItems.add(new MainMenuItem(MenuItemAdapter.MENU_ITEM_INDEX_PROGRAMS, "InaHRS", R.drawable.icon_programs));
    }

    private void setupRecyclerView() {
        mMenuItemRV.setHasFixedSize(true);
        mMenuItemRV.setLayoutManager(new GridLayoutManager(this, 3));
        mMenuItemRV.setAdapter(new MenuItemAdapter(mMainMenuItems));
    }

    public void fetchAlarms() {
        String notifUrl = Config.NOTIF_URL;
        JsonObjectRequest alarmRequest = new JsonObjectRequest(notifUrl,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setAlarmFromJson(response);
                    }
                },
                null // Nggak pakai error response
        );

        VolleySingleton.getInstance(this).addToRequestQueue(alarmRequest);
    }

    private void setAlarmFromJson(JSONObject object) {

        try {

            /**
             * Setting alarms for symposiums.
             */
            JSONArray sympoAlarms = object.getJSONArray("symposia");
            for (int i = 0; i < sympoAlarms.length(); i++) {
                JSONObject alarm = sympoAlarms.getJSONObject(i);
                if (mBookmarkedSymposiumIds.contains(alarm.getInt("id"))) {
                    EventAlarmService.setAlarm(
                            this,
                            alarm.getInt("id") + 100, // +100 buat sympo aja
                            alarm.getString("name"),
                            alarm.getString("date"),
                            alarm.getString("time"),
                            alarm.getString("venue")
                    );
                }
            }

            /**
             * Setting alarms for workshops.
             */
            JSONArray workshopAlarms = object.getJSONArray("workshops");
            for (int i = 0; i < workshopAlarms.length(); i++) {
                JSONObject alarm = workshopAlarms.getJSONObject(i);
                if (mBookmarkedWorkshopIds.contains(alarm.getInt("id"))) {
                    EventAlarmService.setAlarm(
                            this,
                            alarm.getInt("id"),
                            alarm.getString("name"),
                            alarm.getString("date"),
                            alarm.getString("time"),
                            alarm.getString("venue")
                    );
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /* IMPORTANT */
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @OnClick(R.id.sponsorFooter_aphrs)
    void gotoAphrs() {
        Uri uriUrl = Uri.parse("http://www.aphrs.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    @OnClick(R.id.sponsorFooter_perki)
    void gotoPerki() {
        Uri uriUrl = Uri.parse("http://www.inaheart.org/");
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }
}